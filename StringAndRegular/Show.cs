﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StringAndRegular
{
    public static class Show
    {
        /// <summary>
        /// Вывод результата списка url's
        /// </summary>
        /// <param name="html"></param>
        public static void UrlsList(List<string> result)
        {
            if (result.Count > 0)
            {
                foreach (var r in result)
                {

                    Console.WriteLine(r);
                }
            }
            else {
                Console.WriteLine("Вхождений не найдено!");
            }

        }

    
    }
}
