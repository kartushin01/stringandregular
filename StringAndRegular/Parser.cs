﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StringAndRegular
{
    /// <summary>
    /// Получение web страницы
    /// </summary>
    public class Parser
    {
        private WebClient _client = new WebClient();

        public StringBuilder Html = new StringBuilder(); 
        public string Url { get; set; }
        public HttpWebRequest Request { get; set; }
        public HttpStatusCode Code { get; set; }

        public List<string> regExpResult = new List<string>();

        public Parser(string url)
        {
            Url = url;
            GetPage();
        }
        public void GetPage()
        {
            Request = (HttpWebRequest)WebRequest.Create(Url);
            Request.UserAgent = "Mozilla / 5.0(Windows NT 10.0; Win64; x64) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 70.0.3538.77 Safari / 537.36";

            using (var response = (HttpWebResponse)Request.GetResponse())
            {
                Code = response.StatusCode;

                if (Code == HttpStatusCode.OK)
                {
                    using (var sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        string newLine;
                        while ((newLine = sr.ReadLine()) != null)
                            Html.Append(newLine);
                    }

                    ParseRegExp(Html.ToString());
                }                  
            }
        }
        /// <summary>
        /// Поиск вхождений на соответсвие регулярному выражению.
        /// Результат сохраняется в поле List<string> regExpResult.
        /// </summary>
        /// <param name="html"></param>
        public void ParseRegExp(string html)
        {     
            Match m;

            // Схема поиска url
            // <схема>:[//[<логин>[:<пароль>]@]<хост>[:<порт>]][/<URL‐путь>][?<параметры>][#<якорь>]

            var template =
                @"(?'scheme'https?|ftp?):\/\/?(www.)?(?'auth'(?'login'\w*\d*:)?(?'password'[^.]\d*\w*@))?(?'host'[a-zа-яё-]*(?'zone'.[a-zа-яё]+))(?'port':[0-9]{4,})?(?'path'[a-zа-яё0-9\/+.\-?=#]*)";
 
            m = Regex.Match(html, template,
                RegexOptions.IgnoreCase | RegexOptions.Compiled);
            
            while (m.Success) 
            {
                regExpResult.Add(m.Value);
                m = m.NextMatch();
            }    
           
        }

    }
}
